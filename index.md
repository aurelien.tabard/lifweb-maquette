---
layout: default
---


## Objectifs 
- Comprendre la programmation asynchrone en JS
- Comprendre l’architecture d’une application Web en AJAX
- Découvrir l’écosystème des outils à la disposition du programmeur Web
    - environnements de développement
    - plateforme d’exécution / serveur
    - bibliothèques
    - APIs

## Maquette et sujets couverts

### Principes du langage JavaScript (Orienté ES6) et du HTML
- Bloc 1: 
    - Présentation du langage 
    - prog fonctionnelle, closure,  
    - Prototype vs. objet
    - Json
    - Itérateurs
    - Debug + console
- Bloc 2: 
    - Responsive Web Design
    - DOM
    - Event : capture et bubble
    - Debug + console

### Web 1
- Bloc mise en place serveur local
    - Node + Express
    - npm, package.json
    - forge + .gitignore + etc. 
- Mettre en place une application Web côté serveur en utilisant principalement les patterns événementiel et MVC

### DevOps 
- Installer et sécuriser un serveur sur une VM
- Utiliser des outils d’Intégration Continue (CI) et de Déploiement Continu (CD)
- Utiliser les outils côté serveur (console, logs) et côté client (Dev Tools) pour débugger le code

### Web 2
- Templating (ejs.co) ?
- Réaliser une interface Web dynamique à l’aide de scripts côté client (AJAX + fetch + promesses)
- Utiliser une authentification par cookies

## Technos mises en pratique
- Côté serveur : JS / NodeJS / Express / EJS / Passport
- Côté client : Material + JS (vanilla + BOM) + Fetch
- DevOps : Forge, CI, Infra OpenStack du département 

- ce qui était fait en LifIHM : Technos : Bootstrap, DOM, introduction JS et événements
- ce qui était fait en LifAP5 : ?



## Pré-requis (si nécessaire) :
- Bases de 
    - Programmation impérative
    - Programmation fonctionnelle
- Réseau

## Compétences attestées (transversales, spécifiques) :
- Programmer de manière asynchrone en JavaScript
- Concevoir et mettre en place une architecture d’application Web interactive
- Connaître l’écosystème d’outils à la disposition du programmeur Web, et choisir les outils à bon escient : environnements de développement Web, technologies côté serveur, bibliothèques de programmation web, APIs


## Programme de l'UE / Thématiques abordées :

- Programmation JavaScript (ES6) : Présentation du langage, programmation  fonctionnelle, itérateurs, fermetures, notion de prototype et d’objet, format json,  gestion de paquets et de dépendances.
- Programmation dans le navigateur: Arbre DOM, gestion d'évènement.
- Programmation serveur : Nodejs et Express. patterns évènementiels et modèle-vue-contrôleur, authentification.
- DevOps: Gérer son code sur une forge logicielle, installer un serveur sur une Machine Virtuelle, utiliser des outils d’Intégration Continue (CI) et de Déploiement Continu (CD), 
- Outils de debugging pour le web côté serveur (console, logs) et côté client (Dev Tools).
- Programmation Web générique: Templating, communication client-serveur asynchrone.
